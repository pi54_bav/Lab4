﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    public partial class Form1 : Form
    {
        int p1;
        int p2;
        int counter = 0;
        int[,] arr;
        int[,] arr2;

        public Form1()
        {
            InitializeComponent();
            dataGridView1.DefaultCellStyle.Font = new Font("Times New Roman", 12);
            dataGridView2.DefaultCellStyle.Font = new Font("Times New Roman", 12);
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();

            dataGridView2.Rows.Clear();
            dataGridView2.Columns.Clear();

            Random rnd = new Random();

            p1 = Decimal.ToInt32(numericUpDownP1.Value);
            p2 = Decimal.ToInt32(numericUpDownP2.Value);

            arr = new int[p1, p2];
            arr2 = new int[p1, p2];

            for (int i = 0; i < p1; i++)
            {
                for (int j = 0; j < p2; j++)
                {
                    arr[i, j] = rnd.Next(-9, 9);
                    arr2[i, j] = rnd.Next(-9, 9);
                }
            }

            for (int i = 0; i < p1; i++)
            {
                string column = string.Format("{0}", i + 1);
                dataGridView1.Columns.Add(column, column);
                dataGridView2.Columns.Add(column, column);
            }

            for (int j = 0; j < p2; j++)
            {
                string[] currentColumn = new string[p1];

                for (int i = 0; i < p1; i++)
                {
                    currentColumn[i] = arr[i, j].ToString() + "; " + arr2[i, j].ToString();
                }

                dataGridView1.Rows.Add(currentColumn);
                dataGridView2.Rows.Add(currentColumn);
            }

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
            }

            foreach (DataGridViewColumn col in dataGridView1.Columns)
            {
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                col.Width = 60;
                col.SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            foreach (DataGridViewRow row in dataGridView2.Rows)
            {
                row.HeaderCell.Value = String.Format("{0}", row.Index + 1);
            }

            foreach (DataGridViewColumn col in dataGridView2.Columns)
            {
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                col.Width = 60;
                col.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0)
            {
                var cell = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];

                if (e.Button == MouseButtons.Right)
                {
                    cell = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    if (cell.Style.BackColor == Color.PowderBlue)
                    {
                        cell.Style.BackColor = Color.DarkViolet;
                    }
                    else if (cell.Style.BackColor == Color.LightPink)
                    {
                        cell.Style.BackColor = Color.White;
                    }
                    else
                    {
                        cell.Style.BackColor = Color.LightPink;
                    }
                }
                else
                {
                    if (cell.Style.BackColor == Color.LightPink)
                    {
                        cell.Style.BackColor = Color.DarkViolet;
                    }
                    else if (cell.Style.BackColor == Color.PowderBlue)
                    {
                        cell.Style.BackColor = Color.White;
                    }
                    else
                    {
                        cell.Style.BackColor = Color.PowderBlue;
                    }
                    dataGridView1.CurrentCell = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex];
                }
                dataGridView1.ClearSelection();
            }
        }

        private void buttonSolve_Click(object sender, EventArgs e)
        {
            dataGridView2.ClearSelection();

            for (int j = 0; j < p2; j++)
            {
                var max = arr2[0, j];
                var maxi = 0;
                var maxj = j;

                for (int i = 0; i < p1; i++)
                {
                    if (arr2[i, j] > max)
                    {
                        max = arr2[i, j];
                        maxi = i;
                        maxj = j;
                    }
                }
                if (dataGridView2.Rows[maxj].Cells[maxi].Style.BackColor == Color.PowderBlue)
                {
                    dataGridView2.Rows[maxj].Cells[maxi].Style.BackColor = Color.DarkViolet;
                }
                else
                {
                    dataGridView2.Rows[maxj].Cells[maxi].Style.BackColor = Color.LightPink;
                }
            }

            for (int i = 0; i < p1; i++)
            {
                var max = arr[i, 0];
                var maxi = i;
                var maxj = 0;

                for (int j = 0; j < p2; j++)
                {
                    if (arr[i, j] > max)
                    {
                        max = arr[i, j];
                        maxi = i;
                        maxj = j;
                    }
                }
                if (dataGridView2.Rows[maxj].Cells[maxi].Style.BackColor == Color.LightPink)
                {

                    dataGridView2.Rows[maxj].Cells[maxi].Style.BackColor = Color.DarkViolet;
                }
                else
                {
                    dataGridView2.Rows[maxj].Cells[maxi].Style.BackColor = Color.PowderBlue;
                }
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dataGridView1.DefaultCellStyle.Font = new Font("Times New Roman", Decimal.ToInt32(numericUpDown1.Value));
            dataGridView2.DefaultCellStyle.Font = new Font("Times New Roman", Decimal.ToInt32(numericUpDown1.Value));
        }
    }
}
